
var sendBtn;
var chat;
var chatInputWindow;
var socket;
var drawColor;
var penSize;
var penSlider;
var colorSlider;
var data;
const initPenSize = 25;
var initHSBColor;
var sessionID;

function setup() {
	//init environment settings
	var drawingCanvas = createCanvas(windowWidth, windowHeight * 0.8);
	drawingCanvas.parent('canvas');
	colorMode(HSB);
	background(255);
	noStroke();
	cursor(CROSS);
	initHSBColor = random(0,255);
	data = [];

	//draw settings
	penSlider = createSlider(1, 70, initPenSize);
	penSlider.parent('pen-weight');
	colorSlider = createSlider(0, 255, initHSBColor)
	colorSlider.parent('brush-color');

   	//chat variables
	chat = document.getElementById('chat-text');
	chatInputWindow = document.getElementById('chat-input-window');
	
	//DECLARE THE SOCKET > TO HANDLE CONNECTIVITY TO SERVER> 
	socket = io.connect('http://127.0.0.1:8000');
    //sets up an event handler for new connections, to display cached drawing history. 
	
	//***************************SOCKET EVENT HANDLERS************************
	//show drawing history
	socket.on('userDrawingHistory', drawCanvasData);

	//store the session ID locally
	socket.on('yourSessionId', (sessionData) =>{
		sessionID = sessionData;
	});

    //draw new shapes, when new draw data arrives
    socket.on('userMouseInput', drawCanvasData);

	//clear the canvas
	socket.on('clearCanvas', (newCanvasState) => {
    	background(255);
    	drawCanvasData(newCanvasState);
	});
	
	//recieve user messages on the first connection to server
	//userMessageList is an array. each element an array of ['userid', 'message']
	socket.on('userMessages', (usrMessgelist) => {
		var txtNode;
		if(usrMessgelist.length > 0){
			usrMessgelist.forEach((message)=>{
				addUserMessageToChatWindow(message);
			}); 
		}
	});

	//handles the arrival of a new message from another chat user
	socket.on('newUserMessage', (newUserMessage) => {
		addUserMessageToChatWindow(newUserMessage);
	});

	//Add and event listener for the send button. 
	sendBtn = document.getElementById('send-button');
	var userTextInput =  document.getElementById('chat-input-window');
	

	sendBtn.addEventListener('click', function(usersMessage){
		if(usersMessage.value != ""){
			var messageData = [sessionID, userTextInput.value];
			socket.emit('userMessageSent', messageData );
			//this is text the user has entered, so it must be right justified
			addUserMessageToChatWindow(messageData)
			userTextInput.value = "";
			updateScroll(chat);
		}
	});
	sendBtn.addEventListener('keypress', (e)=> {
		if(e.value === 13){
			console.log("Boom");
		}
	});


}    	

function drawCanvasData(drawData){
	for(var i = 0; i < drawData.length; i++){
    	 	fill(drawData[i].color, 255, 255);
    	 	ellipse(drawData[i].x, drawData[i].y, drawData[i].pen)
    	 }
}


function addUserMessageToChatWindow(message){
	//create an HTML node before appending it
	var pTag = document.createElement("p")

	//if the data is from me, set class that can be right justified. 
	if(message[0] == sessionID){
		pTag.className = "myInput";
	}

	txtNode = document.createTextNode(message[1]);
	pTag.appendChild(txtNode);
	chat.appendChild(pTag);
	updateScroll(chat);

}


function mouseDragged(){
	data.push({
		x: mouseX,
		y: mouseY,
		color: drawColor,
		pen: penSize
	});
	
	ellipse(mouseX, mouseY, penSize);
}	

function mouseReleased(){

	//to emit a message to be sent we need
	//1. a name for the message (str)
	//2. data to send (JSON)
	socket.emit('userMouseInput', data);

	//aparently, this is the fastest way to empty an array list,
	//just dereference and create a new one. 
	data = [];
}	

function draw() {	
 	penSize = penSlider.value();
 	drawColor = colorSlider.value();
 	fill(drawColor, 255, 255);

 	document.getElementById("colour-swatch").style.backgroundColor = "hsl("+drawColor+",100%,50%)";

	//show a pen tip to help with drawing
	 //noFill();
	 //stroke(drawColor, 255, 255);
	 //ellipse(mouseX, mouseY, penSize);
}

function clearCanvas(){
	socket.emit('clearCanvas',[]);
} 


//scrolls a div to the bottom of it's size, to allow new content to arrive. 
//should be called whenever a text event is displayed on screen. 
//@param divID - an Element object of the div element to scroll
function updateScroll(divID){
    divID.scrollTop = divID.scrollHeight;
}


/* document.addEventListener('click', function(e) {
    e = e || window.event;
    var target = e.target || e.srcElement;
    console.log(e.target.nodeName);
    if(e.target.nodeName === 'CANVAS'){
    	console.log('boom!');
    }
	}, false);
 */