window.onload=function(){
	var toggleBtn = document.querySelector('.sidebar-toggle');
	var sidebar = document.querySelector('.sidebar');
	var canvas = document.querySelector('.page-content')

	toggleBtn.addEventListener('click', function() {
	  toggleBtn.classList.toggle('is-visible');
	  sidebar.classList.toggle('is-visible');
	  canvas.classList.toggle('is-visible');
	});
}


