//import modules
var express = require('express');
var socket = require('socket.io');

const PORT_NUMBER = 8000;
var webApp = express();
var server = webApp.listen(process.env.port || PORT_NUMBER);

var drawingHistory = [];
var messageHistory = [['Admin', 'Welcome to bennyChat v1.0.'], ['Admin', 'Keep it clean princess...']];

//instruct express to host the static content in public directory
webApp.use(express.static('public'));

//create a socket that forms part of the server
var io = socket(server);
io.sockets.on('connection', (socket) => {
	//send the socketID to the client
	console.log('new client connection: ' + socket.id);
	socket.emit('yourSessionId', socket.id);


	if(drawingHistory.length > 0){
		console.log("Sending drawingHistory - " + drawingHistory.length + " states!");
		socket.emit('userDrawingHistory', drawingHistory);
	}
	socket.emit('userMessages', messageHistory);

	socket.on('userMouseInput', (socketData) => {
		for(var x = 0; x < socketData.length; x++){
			drawingHistory.push(socketData[x]);
		}
		//excludes the client recieving its sent message
		socket.broadcast.emit('userMouseInput', socketData);
		//includes global client connections, including the sender
		//io.sockets.emit('userMouseInput', data);
	});
	socket.on('clearCanvas', () =>{
		drawingHistory = [];
		//console.log("Sending Clear canvas instruction. Drawing history: " + drawingHistory);
		io.sockets.emit('clearCanvas', drawingHistory);
	});

	socket.on('userMessageSent', (userMessage) =>{
		messageHistory.push(userMessage);
		socket.broadcast.emit('newUserMessage', userMessage);

	});
});



console.log(`Socket Server running on port: ${PORT_NUMBER}`);